package main

import (
	"bytes"
	"testing"
)

func TestReadValidSerials(t *testing.T) {
	in := bytes.NewBufferString(`0000AAAA1111BBBB`)
	out, err := readValidSerials(in)
	if err != nil {
		t.Fatal(t)
	}
	if out.Len() == 0 {
		t.Error("No valid serial number were read")
	}
	if out.String() != "0000-AAAA-1111-BBBB\n" {
		t.Error("The output was incorrect")
	}

	in = bytes.NewBufferString(`0000-AAAA-1111-BBBB 1111-BBBB-2222-CCCC`)
	out, err = readValidSerials(in)
	if err != nil {
		t.Fatal(t)
	}
	if out.Len() == 0 {
		t.Error("No valid serial number were read")
	}
	if out.String() != "0000-AAAA-1111-BBBB\n1111-BBBB-2222-CCCC\n" {
		t.Error("The output was incorrect")
	}

	in = bytes.NewBufferString(`0000AAAA1111BBBB 1111BBBB2222CCCC
2222CCCC3333DDDD
3333DDDD4444EEEE
4444EEEE5555FFFF 5555FFFF6666GGGG

6666GGGG7777HHHH
`)
	out, err = readValidSerials(in)
	if err != nil {
		t.Fatal(t)
	}
	if out.Len() == 0 {
		t.Error("No valid serial number were read")
	}
	if out.String() != "0000-AAAA-1111-BBBB\n1111-BBBB-2222-CCCC\n2222-CCCC-3333-DDDD\n3333-DDDD-4444-EEEE\n4444-EEEE-5555-FFFF\n5555-FFFF-6666-GGGG\n6666-GGGG-7777-HHHH\n" {
		t.Error("The output was incorrect")
	}
}

func TestValidSerialsWithDashes(t *testing.T) {
	serials := validSerials(`0000-AAAA-1111-BBBB`)
	if l := len(serials); l != 1 {
		t.Error("Expected 1 valid serial number, got", l)
	}

	serials = validSerials(`0000-AAAA-1111-BBBB 1111-BBBB-2222-CCCC`)
	if l := len(serials); l != 2 {
		t.Error("Expected 2 valid serial numbers, got", l)
	}

	serials = validSerials(`0000-AAAA-1111-BBBB 1111-BBBB-2222-CCCC
2222-CCCC-3333-DDDD
3333-DDDD-4444-EEEE
4444-EEEE-5555-FFFF 5555-FFFF-6666-GGGG

6666-GGGG-7777-HHHH
`)
	if l := len(serials); l != 7 {
		t.Error("Expected 7 valid serial numbers, got", l)
	}
}

func TestValidSerialsWithoutDashes(t *testing.T) {
	serials := validSerials(`0000AAAA1111BBBB`)
	if l := len(serials); l != 1 {
		t.Error("Expected 1 valid serial number, got", l)
	}

	serials = validSerials(`0000AAAA1111BBBB 1111BBBB2222CCCC`)
	if l := len(serials); l != 2 {
		t.Error("Expected 2 valid serial numbers, got", l)
	}

	serials = validSerials(`0000AAAA1111BBBB 1111BBBB2222CCCC
2222CCCC3333DDDD
3333DDDD4444EEEE
4444EEEE5555FFFF 5555FFFF6666GGGG

6666GGGG7777HHHH
`)
	if l := len(serials); l != 7 {
		t.Error("Expected 7 valid serial numbers, got", l)
	}
}
