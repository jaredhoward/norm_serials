package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

var (
	input    = flag.String("i", "stdin", "the input file to read from")
	output   = flag.String("o", "stdout", "the output file to save to")
	validNum = regexp.MustCompile(`\b(\w{4})-?(\w{4})-?(\w{4})-?(\w{4})\b`)
	nl       = "\n"
)

func main() {
	flag.Parse()

	// Setup the input file
	in, err := file(*input, false)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Input file couldn't be opened (%s): %s\n", *input, err)
		return
	}
	defer in.Close()

	// Read the input file and store the serial numbers in the buffer
	r, err := readValidSerials(in)
	if err != nil {
		fmt.Fprintf(os.Stderr, "An error occurred while reading the serial numbers: %s\n", *input, err)
		return
	}
	if r.Len() == 0 {
		fmt.Fprintln(os.Stderr, "No valid serial numbers were found.")
		return
	}
	in.Close()

	// Setup the output file
	out, err := file(*output, true)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Output file couldn't be opened/created (%s): %s\n", *output, err)
		return
	}
	defer out.Close()

	out.Write(r.Bytes())
}

func file(filename string, create bool) (*os.File, error) {
	switch filename {
	case "stdin":
		return os.Stdin, nil
	case "stdout":
		return os.Stdout, nil
	default:
		var file *os.File
		var err error
		if create {
			file, err = os.Create(filename)
		} else {
			file, err = os.Open(filename)
		}
		if err != nil {
			return nil, err
		}
		return file, nil
	}
}

func readValidSerials(rd io.Reader) (bytes.Buffer, error) {
	var b bytes.Buffer

	in := bufio.NewReader(rd)
	for {
		l, err := in.ReadString('\n')
		if len(l) > 0 {
			serials := validSerials(l)
			for _, serial := range serials {
				b.WriteString(serial + nl)
			}
		}
		if err != nil {
			if err == io.EOF {
				break
			}
			return b, err
		}
	}

	return b, nil
}

func validSerials(str string) []string {
	serials := make([]string, 0)

	// Find all the valid serial numbers.
	nums := validNum.FindAllStringSubmatch(str, -1)
	for _, num := range nums {
		serials = append(serials, strings.Join(num[1:5], "-"))
	}
	return serials
}
