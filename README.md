# Normalize Serial Numbers

My wife's job requires serial numbers to be manipulated from a given list to a particular format.
This tool is to help her in that process.
